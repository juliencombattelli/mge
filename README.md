# Makuyop Game Engine - MGE

This is an open source C++ game engine project, based on SFML library.

The goal of this project is both create a 2D RPG game (like Pokemon, Zelda or Final Fantasy) and a reusable game engine for any 2D game.

This project use the latest C++ features, including C++20. Thus, a very recent compiler is needed. 
The recommended one is GCC9+, along with CMake 3.12+. You will also need the SFML 2.5 library.
