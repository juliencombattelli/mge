#ifndef MGE_COMPONENTPOOL_HPP_
#define MGE_COMPONENTPOOL_HPP_

#include <vector>

namespace mge {
class BaseComponentPool {};

template <typename Component>
class ComponentPool : BaseComponentPool {
public:
    using container_type = std::vector<Component>;

    using size_type = typename container_type::size_type;
    using reference = typename container_type::reference;
    using const_reference = typename container_type::const_reference;

    size_type size() const { return _components.size(); }

    void resize(size_type size) { _components.resize(size); }

    reference at(size_type pos) { return _components.at(pos); }
    const_reference at(size_type pos) const { return _components.at(pos); };

    reference operator[](size_type pos) { return _components[pos]; }
    const_reference operator[](size_type pos) const { return _components[pos]; };

    template <typename... Args>
    reference construct(size_type pos, Args&&... args) {
        reference component = _components.at(pos);
        ::new (std::addressof(component)) Component{std::forward<Args>(args)...};
        return component;
    }

    void destroy(size_type pos) {
        reference component = _components.at(pos);
        component.~Component();
    }

private:
    container_type _components;
};
}  // namespace mge

#endif  // MGE_COMPONENTPOOL_HPP_