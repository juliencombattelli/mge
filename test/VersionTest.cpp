#include <gtest/gtest.h>

#include <mge/Version.hpp>

TEST(VersionTest, MajorMinorPatchNumber) {
    EXPECT_EQ(mge::kCurrentVersion.major, MGE_VERSION_MAJOR);
    EXPECT_EQ(mge::kCurrentVersion.minor, MGE_VERSION_MINOR);
    EXPECT_EQ(mge::kCurrentVersion.patch, MGE_VERSION_PATCH);
}