#include <array>
#include <optional>
#include <stdexcept>
#include <string_view>
#include <type_traits>

using namespace std::literals;

namespace fmt {

template <typename T, std::size_t TMaxSize>
struct static_vector {
    using array_type = std::array<T, TMaxSize>;

    constexpr inline static std::size_t _max_size = TMaxSize;

public:
    using value_type = typename array_type::value_type;
    using size_type = typename array_type::size_type;
    using difference_type = typename array_type::difference_type;
    using reference = typename array_type::reference;
    using const_reference = typename array_type::const_reference;
    using pointer = typename array_type::pointer;
    using const_pointer = typename array_type::const_pointer;
    using iterator = typename array_type::iterator;
    using const_iterator = typename array_type::const_iterator;
    using reverse_iterator = typename array_type::reverse_iterator;
    using const_reverse_iterator = typename array_type::const_reverse_iterator;

    constexpr iterator begin() noexcept { return iterator{data()}; }
    constexpr const_iterator begin() const noexcept { return const_iterator{data()}; }
    constexpr const_iterator cbegin() const noexcept { return const_iterator{data()}; }

    constexpr iterator end() noexcept { return iterator{data() + _count}; }
    constexpr const_iterator end() const noexcept { return const_iterator{data() + _count}; }
    constexpr const_iterator cend() const noexcept { return const_iterator{data() + _count}; }

    constexpr reverse_iterator rbegin() noexcept { return reverse_iterator{end()}; }
    constexpr const_reverse_iterator rbegin() const noexcept {
        return const_reverse_iterator{end()};
    }
    constexpr const_reverse_iterator crbegin() const noexcept {
        return const_reverse_iterator{end()};
    }

    constexpr reverse_iterator rend() noexcept { return reverse_iterator{begin()}; }
    constexpr const_reverse_iterator rend() const noexcept {
        return const_reverse_iterator{begin()};
    }
    constexpr const_reverse_iterator crend() const noexcept {
        return const_reverse_iterator{begin()};
    }

    constexpr reference operator[](size_type pos) { return _array[pos]; }
    constexpr const_reference operator[](size_type pos) const { return _array[pos]; }

    constexpr reference at(size_type pos) {
        if (pos >= _count) {
            throw std::out_of_range("static_vector");
        }
        return _array[pos];
    }
    constexpr const_reference at(size_type pos) const {
        if (pos >= _count) {
            throw std::out_of_range("static_vector");
        }
        return _array[pos];
    }

    constexpr T* data() noexcept { return _array.data(); }
    constexpr const T* data() const noexcept { return _array.data(); }

    [[nodiscard]] constexpr bool empty() const noexcept { return _array.empty(); }
    [[nodiscard]] constexpr size_type size() const noexcept { return _count; }
    [[nodiscard]] constexpr size_type max_size() const noexcept { return _max_size; }

    constexpr void add(const value_type& value) {
        if (_count >= _max_size) {
            throw std::out_of_range("static_vector");
        }
        _array[_count++] = value;
    }

    constexpr void swap(static_vector& other) noexcept(std::is_nothrow_swappable_v<T>) {
        using std::swap;
        swap(_count, other._count);
        swap(_array, other._array);
    }

private:
    std::size_t _count{0};
    std::array<T, _max_size> _array;
};

template <class T, std::size_t TMaxSize>
constexpr void swap(static_vector<T, TMaxSize>& lhs,
                    static_vector<T, TMaxSize>& rhs) noexcept(noexcept(lhs.swap(rhs))) {
    lhs.swap(rhs);
}

///////////////////////////////////////////////////////////////////////////////
// A parser for T is a callable thing that takes a "string" and returns
// (optionally) an T plus the "leftover string".
using parse_input = std::string_view;
template <typename T>
using parse_result = std::pair<T, parse_input>;
template <typename T>
using opt_parse_result = std::optional<parse_result<T>>;
///////////////////////////////////////////////////////////////////////////////

// Maximum replacement field a format string can contain
constexpr std::size_t max_repl_field_count = 4;

using repl_field_list = static_vector<std::string_view, max_repl_field_count>;

namespace detail {

template <typename F, typename TAcc, typename T>
inline constexpr bool is_accumulator_v = std::is_invocable_r_v<TAcc, F, TAcc&, T>;

template <typename TParser, typename T, typename TAccumulator>
constexpr parse_result<T> accumulate_parse(parse_input input, TParser&& parser, T init,
                                           TAccumulator&& accumulator) {
    static_assert(is_accumulator_v<TAccumulator, T, parse_input>);

    while (not input.empty()) {
        const auto result = parser(input);
        if (not result) {
            return std::make_pair(init, input);
        }
        init = accumulator(init, result->first);
        input = result->second;
    }
    return std::make_pair(init, input);
}

template <typename TParser, typename T, typename TAccumulator>
constexpr parse_result<T> accumulate_n_parse(parse_input input, TParser&& parser, std::size_t n,
                                             T init, TAccumulator&& accumulator) {
    static_assert(is_accumulator_v<TAccumulator, T, parse_input>);

    while (n != 0) {
        const auto result = parser(input);
        if (not result) {
            return std::make_pair(init, input);
        }
        init = accumulator(init, result->first);
        input = result->second;
        --n;
    }
    return std::make_pair(init, input);
}

}  // namespace detail

/**
 * \function zero_or_more
 * \brief Apply zero or more time the parser p until it fails
 */
template <typename P, typename T, typename F>
constexpr auto zero_or_more(P&& p, T&& init, F&& f) {
    return [p, init, f](parse_input s) {
        return opt_parse_result<T>(detail::accumulate_parse(s, p, init, f));
    };
}

constexpr auto parse_format_string(const std::string_view& s) {
    constexpr auto format_pattern = [](parse_input input) {
        return opt_parse_result<parse_input>{std::make_pair("a"sv, input)};
    };
    // format_pattern_parser();
    return zero_or_more(format_pattern, repl_field_list{},
                        [](repl_field_list& acc, parse_input str) {
                            // TODO add str to acc
                            return acc;
                        });
}

constexpr void format(const std::string_view& s) {
    const auto repl_fields = parse_format_string(s);
}

}  // namespace fmt

void test_parser() {
    fmt::static_vector<std::string_view, 4> list;
    // fmt::format("sdc{hjjhqbd}sd d{hello}{rehello}"sv);
}

int main() {
    constexpr auto lambda = [](fmt::repl_field_list acc, fmt::parse_input) { return acc; };
    static_assert(std::is_invocable_r_v<fmt::repl_field_list, decltype(lambda),
                                        fmt::repl_field_list, fmt::parse_input>);
    return 0;
}